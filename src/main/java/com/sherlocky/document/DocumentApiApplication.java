package com.sherlocky.document;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: zhangcx
 * @date: 2019/8/8 16:26
 */
@SpringBootApplication
public class DocumentApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(DocumentApiApplication.class, args);
    }
}
